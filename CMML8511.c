﻿/*
 * CMML8511.c
 *
 * Created: 25.02.2017 23:44:07
 *  Author: Christian Möllers
 */ 

#include <avr/io.h>

#include "../CMGlobal.h"
#include "CMML8511.h"
#include "../CMAdcMessure/CMAdcMessure.h"

//The Arduino Map function but for floats
//From: http://forum.arduino.cc/index.php?topic=3922.0
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/**
	@brief	ML8511 UV-Sensor, will messure a uv value
	@param 	adc volt measurement channel in digit, adc sensor measurement channel
	@return	float value in uv value
*/
float readML8511Sensor(uint8_t adcVoltChannel, uint8_t adcSensorChannel)
{
	float ml8511uvValue = 0.0;
	
	ml8511VoltRefernce_INPUT;
	ml8511Sensor_INPUT;
	
	int ml8511adcValue = adcMessure(adcSensorChannel);
	int ml8511adcVoltValue = adcMessure(adcVoltChannel);

	//Use the 3.3V power pin as a reference to get a very accurate output value from sensor
	float outputVoltage = 3.3 / ml8511adcVoltValue * ml8511adcValue;

	//Convert the voltage to a UV intensity level
	ml8511uvValue = mapfloat(outputVoltage, 0.99, 2.8, 0.0, 15.0); 
	
	return ml8511uvValue;
}

