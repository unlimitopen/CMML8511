﻿/*
 * CMML8511.h
 *
 * Created: 25.02.2017 23:44:41
 *  Author: Christian Möllers
 */ 


#ifndef CMML8511_H_
#define CMML8511_H_

//  ML8511 Sensor Pin definition
#define ml8511Sensor			2
#define ml8511Sensor_PIN		PC2
#define ml8511Sensor_PORT		PORTC
#define ml8511Sensor_PINR		PINC
#define ml8511Sensor_OUTPUT    (DDRC |= (1<<ml8511Sensor_PIN))
#define ml8511Sensor_INPUT     (DDRC &=~ (1<<ml8511Sensor_PIN))
#define ml8511Sensor_HI        (PORTC |= (1<<ml8511Sensor_PIN))
#define ml8511Sensor_LOW       (PORTC &=~ (1<<ml8511Sensor_PIN))
#define ml8511Sensor_TOG       (PORTC ^= (1<<ml8511Sensor_PIN))
#define ml8511Sensor_IN        (ml8511Sensor_PINR&(1<<ml8511Sensor_PIN) ? 1 : 0 )

//  ML8511 Sensor Pin definition
#define ml8511VoltRefernce			 3
#define ml8511VoltRefernce_PIN		 PC3
#define ml8511VoltRefernce_PORT		 PORTC
#define ml8511VoltRefernce_PINR		 PINC
#define ml8511VoltRefernce_OUTPUT    (DDRC |= (1<<ml8511VoltRefernce_PIN))
#define ml8511VoltRefernce_INPUT     (DDRC &=~ (1<<ml8511VoltRefernce_PIN))
#define ml8511VoltRefernce_HI        (PORTC |= (1<<ml8511VoltRefernce_PIN))
#define ml8511VoltRefernce_LOW       (PORTC &=~ (1<<ml8511VoltRefernce_PIN))
#define ml8511VoltRefernce_TOG       (PORTC ^= (1<<ml8511VoltRefernce_PIN))
#define ml8511VoltRefernce_IN        (ml8511VoltRefernce_PINR&(1<<ml8511VoltRefernce_PIN) ? 1 : 0 )


float readML8511Sensor(uint8_t adcVoltChannel, uint8_t adcSensorChannel);


#endif /* CMML8511_H_ */